from flask import Flask, jsonify, request
from db import *
from bson.objectid import ObjectId
app = Flask(__name__)


@app.route('/')
def hello():
    return "Hello World!"

@app.route('/api/v1/tools', methods=["GET"])
def get_tools():
	"""
	@api {get} /tools Request an index of all technologies' resources
	@apiName GetTools
	@apiGroup Tools

	@apiSuccess {String} title Title of the resource (mandatory).
	@apiSuccess {String} description  Description of the resource (optional).
	@apiSuccess {String} resource  Link toward the resource (mandatory).
	@apiSuccess {String} author  Author of the resource (mandatory).
	@apiSuccess {Array} tags  Tags related to the resource (optional).


	"""
	tools = db.tools.find()
	print(tools)
	tests = []
	for document in tools:
		document['_id'] = str(document['_id'])
		tests.append(document)
	return jsonify({'tools': tests})

@app.route('/api/v1/tools', methods=["POST"])
def create_tools():
	"""
	@api {post} /tools Adds a new Resource
	@apiVersion 1.0.0
	@apiName createResource
	@apiGroup Tools

	@apiParam {String}      title        The resource's title.
	@apiParam {String}      description      The resource's description(optional).
	@apiParam {String}      author       The resource's author.
	@apiParam {String}      resource         The resource's link.
	@apiParam {Array}      tags    The resource's tags (optional).

	@apiSuccess {Number}    id              The new resource id.
	"""
	if request.args['title']!=None and request.args['author']!=None and request.args['resource']!=None:

		resTitle=request.args['title']
		resDesc=request.args['description']
		resRes=request.args['resource']
		resAuthor=request.args['author']
		resTags=request.args['tags']

		try:
			db.tools.insert({'title':resTitle, 'description':resDesc, 'resource':resRes, 'author':resTags, 'tags':resTags})
		except:
			print('failed to push in db')

		return "Success in creating a new resource"
	else:
		return "You must provide a title an author and a resource. Feel free to add a description and a collection of tags."


@app.route('/api/v1/tools/<string:id>', methods=["GET"])
def get_tool(id):
	try:
		tool=db.tools.find_one({'_id': ObjectId(id)})
		print(tool)
	except:
		print('failed to find resource')

	

	toolTweaked= tool
	toolTweaked['_id'] = id
	print("==============={}==========".format(toolTweaked))
	
	return jsonify({'tool': toolTweaked})

	

@app.route('/api/v1/tools/<string:id>', methods=[ "DELETE"])
def delete_tool(id):
	"""
	@api {delete} /tools:id/ Delete a  Resource
	@apiVersion 1.0.0
	@apiName deleteResource
	@apiGroup Tools

	@apiParam {String}      id        The resource's id.

    
	"""
	try:
		tool=db.tools.find_one({'_id': ObjectId(id)})
		print(tool)
	except:
		print('failed to find resource')


	try:
		db.tools.delete_one({'_id': ObjectId(id) })
	except:
		print('failed to destroy resource')
	return "Delete successfull"


app.route('/api/v1/tools/<string:id>', methods=["PUT"])
def update_tool(id):
	"""
	@api {put} /tools:id/ Update a  Resource
	@apiVersion 1.0.0
	@apiName updateResource
	@apiGroup Tools

	@apiParam {String}      title        The resource's title.
	@apiParam {String}      description      The resource's description(optional).
	@apiParam {String}      author       The resource's author.
	@apiParam {String}      resource         The resource's link.
	@apiParam {Array}       tags    The resource's tags (optional).
    
	"""
	if request.args['title']!=None and request.args['author']!=None and request.args['resource']!=None:

		try:
			tool=db.tools.find_one({'_id': ObjectId(id)})
			print(tool)
		except:
			print('failed to find resource')
		
		resourceTitle=request.args['title']
		resourceDesc=request.args['description']
		resourceAuthor=request.args['author']
		resourceLink=request.args['resource']
		resourceTags=request.args['tags']

		try:
			db.tools.update({'_id': id },{"$set" : {'title': resourceTitle, 'description': resourceDesc, 'author': resourceAuthor, 'resource': resourceLink, 'tags': resourceTags} })

		except:
			print('failed to update')

		return "Update successfull"
	else:
		return "You must provide a title an author and a resource. Feel free to add a description and a collection of tags."



if __name__ == '__main__':
    app.run(debug=True)


