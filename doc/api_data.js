define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "/home/elodie/Documents/flaskapi/doc/main.js",
    "groupTitle": "/home/elodie/Documents/flaskapi/doc/main.js",
    "name": ""
  },
  {
    "type": "get",
    "url": "/tools",
    "title": "Request an index of all technologies' resources",
    "name": "GetTools",
    "group": "Tools",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title of the resource (mandatory).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description of the resource (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resource",
            "description": "<p>Link toward the resource (mandatory).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "author",
            "description": "<p>Author of the resource (mandatory).</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "tags",
            "description": "<p>Tags related to the resource (optional).</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./app.py",
    "groupTitle": "Tools"
  },
  {
    "type": "post",
    "url": "/tools",
    "title": "Adds a new Resource",
    "version": "1.0.0",
    "name": "createResource",
    "group": "Tools",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The resource's title.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>The resource's description(optional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "author",
            "description": "<p>The resource's author.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "resource",
            "description": "<p>The resource's link.</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "tags",
            "description": "<p>The resource's tags (optional).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The new resource id.</p>"
          }
        ]
      }
    },
    "filename": "./app.py",
    "groupTitle": "Tools"
  },
  {
    "type": "delete",
    "url": "/tools:id/",
    "title": "Delete a  Resource",
    "version": "1.0.0",
    "name": "deleteResource",
    "group": "Tools",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>The resource's id.</p>"
          }
        ]
      }
    },
    "filename": "./app.py",
    "groupTitle": "Tools"
  },
  {
    "type": "put",
    "url": "/tools:id/",
    "title": "Update a  Resource",
    "version": "1.0.0",
    "name": "updateResource",
    "group": "Tools",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The resource's title.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>The resource's description(optional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "author",
            "description": "<p>The resource's author.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "resource",
            "description": "<p>The resource's link.</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "tags",
            "description": "<p>The resource's tags (optional).</p>"
          }
        ]
      }
    },
    "filename": "./app.py",
    "groupTitle": "Tools"
  }
] });
