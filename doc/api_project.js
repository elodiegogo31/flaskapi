define({
  "name": "My techy tools",
  "version": "0.1.0",
  "description": "Central storage for all the infos I want to hit up",
  "title": "Custom apiDoc browser title",
  "url": "https://api.github.com/v1",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-05-04T13:29:38.190Z",
    "url": "http://apidocjs.com",
    "version": "0.22.0"
  }
});
